package com.lmccarty.restexample.fragments;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.lmccarty.restexample.Main;
import com.lmccarty.restexample.R;
import com.lmccarty.utils.BitmapLruCache;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Details extends Fragment {

	private static final String TAG = "Details";
	private static final String SPACER = " ";

	private RelativeLayout rlData;
	private TextView txtTitle;
	private TextView txtNetwork;
	private TextView txtDetails;
	private TextView txtDuration;
	private TextView txtRating;
	private TextView txtEmpty;

	private String title = "";
	private String description = "";
	private String poster = "";
	private String networkName = "";
	private String duration = "0";
	private String rating = "";

	private RequestQueue queue;
	private NetworkImageView image;
	private ImageLoader imgLoader;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		queue = Volley.newRequestQueue(getActivity());
		imgLoader = new ImageLoader(queue, new BitmapLruCache());

		if (savedInstanceState != null) {
			title = savedInstanceState.getString(Main.TITLE);
			networkName = savedInstanceState.getString(Main.NETWORK_NAME);
			poster = savedInstanceState.getString(Main.POSTER_ART);
			description = savedInstanceState.getString(Main.VIDEO_DETAILS);
			duration = savedInstanceState.getString(Main.VIDEO_DURATION);
			rating = savedInstanceState.getString(Main.VIDEO_RATING);
		}

		if (getArguments() != null) { // This is a phone
			Bundle bundle = this.getArguments();

			String[] values = bundle.getStringArray(Main.VIDEO_DETAIL_VIEW);

			title = values[0];
			description = values[1];
			poster = values[2];
			networkName = values[3];
			duration = values[4];
			rating = values[5];
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(
				com.lmccarty.restexample.R.layout.panel_details, container,
				false);
		rlData = (RelativeLayout) view.findViewById(R.id.rlData);

		image = (NetworkImageView) view.findViewById(R.id.imgPosterArt);

		txtTitle = (TextView) view.findViewById(R.id.txtTitle);
		txtNetwork = (TextView) view.findViewById(R.id.txtNetworkName);
		txtDetails = (TextView) view.findViewById(R.id.txtDescription);
		txtDuration = (TextView) view.findViewById(R.id.txtDuration);
		txtRating = (TextView) view.findViewById(R.id.txtRating);
		txtEmpty = (TextView) view.findViewById(R.id.txtEmpty);
		return view;
	}

	@Override
	public void onStart() {
		super.onStart();

		if (!title.isEmpty())
			setupUI(title, description, poster, networkName, duration, rating);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		queue.cancelAll(this);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(Main.TITLE, title);
		outState.putString(Main.POSTER_ART, poster);
		outState.putString(Main.NETWORK_NAME, networkName);
		outState.putString(Main.VIDEO_DETAILS, description);
		outState.putString(Main.VIDEO_DURATION, duration);
		outState.putString(Main.VIDEO_RATING, rating);
	}

	public void setupUI(String... params) {
		title = params[0];
		description = params[1];
		poster = params[2];
		networkName = params[3];
		duration = params[4];
		rating = params[5];

		txtTitle.setText(title);
		txtNetwork.setText(networkName + SPACER);
		txtDetails.setText(description);
		txtDuration.setText(convertSecondsToMinutes(duration) + SPACER);
		txtRating.setText(SPACER + rating);
		rlData.setVisibility(View.VISIBLE);

		image.setImageUrl(poster, imgLoader);

		if (txtEmpty.getVisibility() == View.VISIBLE) {
			txtEmpty.setVisibility(View.GONE);
		}

		if (rlData.getVisibility() != View.VISIBLE) {
			rlData.setVisibility(View.VISIBLE);
		}
	}

	/**
	 * @param duration
	 *            of the video in seconds
	 * @return String number of seconds converted to minutes
	 */
	private String convertSecondsToMinutes(String duration) {
		int time = 0;
		try {
			int dur = Integer.parseInt(duration);
			time = dur / 60;
		} catch (NumberFormatException e) {
			Log.e(TAG, "Not an integer, retunr 0 instead: " + e.getMessage());
		}
		return String.valueOf(time);
	}
}