package com.lmccarty.restexample.fragments;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.lmccarty.restexample.R;
import com.lmccarty.utils.BitmapLruCache;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class DataList extends Fragment implements OnChildClickListener,
		OnGroupCollapseListener, OnGroupExpandListener {

	private static final String TAG = "DataList";
	private static final String URL = "http://xfinitytv.comcast.net/api/xfinity/ipad/home/videos?filter&type=json";

	private static final String HEADER = "header";
	private static final String HEADER_DESCRIPTION = "description";
	private static final String NETWORK_NAME = "networkName";
	private static final String POSTER_ART_URL = "entityPosterArtUrl";
	private static final String PROGRAM_NAME = "name";
	private static final String THUMB_URL = "entityThumbnailUrl";
	private static final String VIDEO_ITEMS = "items";
	private static final String VIDEO_DESCRIPTION = "videoDescription";
	private static final String VIDEO_DURATION = "videoDuration";
	private static final String VIDEO_GALLERIES = "videoGalleries";
	private static final String VIDEO_RATING = "videoRating";

	private static final String UNKNOWN_RATING = "-";

	private static final String CHILD = "child";
	private static final String GROUP_OPEN = "groupOpen";
	private static final String CHILD_SELECTED = "childSelected";

	private List<String> listVideoType = new ArrayList<String>(0);
	private List<String> listHeaderDescripton = new ArrayList<String>(0);
	private List<List<String>> listVideoName = new ArrayList<List<String>>(0);
	private List<List<String>> listNetworkName = new ArrayList<List<String>>(0);
	private List<List<String>> listPosterArt = new ArrayList<List<String>>(0);
	private List<List<String>> listVideoRating = new ArrayList<List<String>>(0);
	private List<List<String>> listThumb = new ArrayList<List<String>>(0);
	private List<List<String>> listVideoDescription = new ArrayList<List<String>>(
			0);
	private List<List<String>> listVideoDuration = new ArrayList<List<String>>(
			0);

	private boolean[] arrayGroupOpen;
	private int child = 0;

	private boolean childWasSelected = false;

	private RequestQueue queue;
	private ExpandableListAdapter adapter;
	private ExpandableListView list;

	OnListItemClicked mCallback;

	public interface OnListItemClicked {
		public void listItemClicked(String... params);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mCallback = (OnListItemClicked) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnListItemClicked");
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (savedInstanceState != null) {
			child = savedInstanceState.getInt(CHILD);
			childWasSelected = savedInstanceState.getBoolean(CHILD_SELECTED);
			arrayGroupOpen = savedInstanceState.getBooleanArray(GROUP_OPEN);
		}

		adapter = new VideoExpandableListAdapter();
		queue = Volley.newRequestQueue(getActivity());

		getData();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(
				com.lmccarty.restexample.R.layout.expandable_list, container,
				false);

		list = (ExpandableListView) view.findViewById(R.id.exp_list);
		list.setOnChildClickListener(this);
		list.setOnGroupCollapseListener(this);
		list.setOnGroupExpandListener(this);

		list.setTextFilterEnabled(true);

		return view;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// Cancel Volley request
		queue.cancelAll(this);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(CHILD, child);
		outState.putBoolean(CHILD_SELECTED, childWasSelected);
		outState.putBooleanArray(GROUP_OPEN, arrayGroupOpen);
	}

	/**
	 * Get JSON data, parse, then fill the listview
	 */
	private void getData() {

		JsonObjectRequest objRequest = new JsonObjectRequest(
				Request.Method.GET, URL, null,

				new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {
						ProgressBar progress = (ProgressBar) getActivity()
								.findViewById(R.id.progList);
						if (progress.getVisibility() != View.VISIBLE)
							progress.setVisibility(View.VISIBLE);

						try {
							jsonParser(response);
							list.setAdapter(adapter);

							int size = listVideoType.size();
							if (arrayGroupOpen != null) {
								if (childWasSelected) {
									for (int i = 0; i < size; i++) {
										if (arrayGroupOpen[i]) {
											list.expandGroup(i);
											list.setSelectedChild(i, child,
													true);
										}
									}
								} else {
									for (int i = 0; i < size; i++) {
										if (arrayGroupOpen[i]) {
											list.expandGroup(i);
										} else {
											list.collapseGroup(i);
										}
									}
								}
							} else {
								arrayGroupOpen = new boolean[size];
								for (int i = 0; i < size; i++) {
									arrayGroupOpen[i] = false;
								}
							}
						} catch (JsonSyntaxException e) {
							Log.e(TAG, "ERROR: " + e.getMessage());
						} finally {
							if (progress.getVisibility() == View.VISIBLE)
								progress.setVisibility(View.GONE);
						}
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Log.e(TAG, "RESPONSE ERROR: " + error.toString());

						ProgressBar progress = (ProgressBar) getActivity()
								.findViewById(R.id.progList);
						if (progress.getVisibility() == View.VISIBLE) {
							progress.setVisibility(View.GONE);

							int duration = Toast.LENGTH_LONG;
							Toast toast = Toast.makeText(
									getActivity(),
									getActivity().getResources().getString(
											R.string.error_gallery), duration);
							toast.show();
						}
					}
				});
		queue.add(objRequest);
	}

	public class VideoExpandableListAdapter extends BaseExpandableListAdapter {
		public Object getChild(int groupPosition, int childPosition) {
			return listVideoName.get(groupPosition).get(childPosition);
		}

		public Object getChildSecondRow(int groupPosition, int childPosition) {
			return listNetworkName.get(groupPosition).get(childPosition);
		}

		public Object getChildThumb(int groupPosition, int childPosition) {
			return listThumb.get(groupPosition).get(childPosition);
		}

		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}

		public int getChildrenCount(int groupPosition) {
			return listVideoName.get(groupPosition).size();
		}

		public View getChildView(int groupPosition, int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {

			LayoutInflater inflater = (LayoutInflater) getActivity()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			View row = inflater.inflate(R.layout.expandable_list_child_item,
					parent, false);

			TextView txtChild = (TextView) row.findViewById(R.id.txtChild);
			txtChild.setText(getChild(groupPosition, childPosition).toString());

			TextView txtNetwork = (TextView) row.findViewById(R.id.txtNetwork);
			txtNetwork.setText(getChildSecondRow(groupPosition, childPosition)
					.toString());

			NetworkImageView image = (NetworkImageView) row
					.findViewById(R.id.imgListThumb);

			ImageLoader imageLoader = new ImageLoader(queue,
					new BitmapLruCache());

			image.setImageUrl(getChildThumb(groupPosition, childPosition)
					.toString(), imageLoader);

			return row;
		}

		public String getGroup(int groupPosition) {
			return listVideoType.get(groupPosition);
		}

		public String getGroupDescription(int groupPosition) {
			return listHeaderDescripton.get(groupPosition);
		}

		public int getGroupCount() {
			return listVideoType.size();
		}

		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) getActivity()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			View row = inflater.inflate(R.layout.expandable_list_parent_item,
					parent, false);
			TextView txtTitle = (TextView) row.findViewById(R.id.txtTitle);
			txtTitle.setText(getGroup(groupPosition));

			TextView txtDescription = (TextView) row
					.findViewById(R.id.txtDescription);
			txtDescription.setText(getGroupDescription(groupPosition));

			return row;
		}

		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}

		public boolean hasStableIds() {
			return true;
		}
	}

	@Override
	public boolean onChildClick(ExpandableListView arg0, View arg1, int arg2,
			int arg3, long arg4) {
		child = arg3;
		childWasSelected = true;
		arrayGroupOpen[arg2] = true;

		String description = listVideoDescription.get(arg2).get(arg3);
		String title = listVideoName.get(arg2).get(arg3);
		String network = listNetworkName.get(arg2).get(arg3);
		String duration = listVideoDuration.get(arg2).get(arg3);

		String posterArt = listPosterArt.get(arg2).get(arg3);

		String rating = listVideoRating.get(arg2).get(arg3);

		mCallback.listItemClicked(title, description, posterArt, network,
				duration, rating);
		return false;
	}

	@Override
	public void onGroupCollapse(int pos) {
		arrayGroupOpen[pos] = false;
	}

	@Override
	public void onGroupExpand(int pos) {
		arrayGroupOpen[pos] = true;
	}

	/**
	 * Parses JSON object and fills lists with data to display in listview
	 * 
	 * @param json
	 * @throws JsonSyntaxException
	 */
	private void jsonParser(JSONObject json) throws JsonSyntaxException {
		JsonParser parser = new JsonParser();
		JsonObject jsonObject = parser.parse(json.toString()).getAsJsonObject();
		JsonArray jsonArray = jsonObject.get(VIDEO_GALLERIES).getAsJsonArray();

		int jsonArraySize = jsonArray.size();
		List<String> vidTypeHeader = new ArrayList<String>(jsonArraySize);
		List<String> descriptionHeader = new ArrayList<String>(jsonArraySize);
		List<List<String>> vidName = new ArrayList<List<String>>(jsonArraySize);
		List<List<String>> networkName = new ArrayList<List<String>>(
				jsonArraySize);

		List<List<String>> posterArt = new ArrayList<List<String>>(
				jsonArraySize);
		List<List<String>> vidRating = new ArrayList<List<String>>(
				jsonArraySize);
		List<List<String>> vidThumb = new ArrayList<List<String>>(jsonArraySize);
		List<List<String>> vidDescription = new ArrayList<List<String>>(
				jsonArraySize);
		List<List<String>> vidDuration = new ArrayList<List<String>>(
				jsonArraySize);

		for (JsonElement jArray : jsonArray) {
			JsonObject tvObject = jArray.getAsJsonObject();
			JsonObject headerObject = tvObject.get(HEADER).getAsJsonObject();

			String programType = headerObject.has(PROGRAM_NAME) ? headerObject
					.get(PROGRAM_NAME).getAsString() : "";
			vidTypeHeader.add(programType);

			String sHeadDescription = headerObject.has(HEADER_DESCRIPTION) ? headerObject
					.get(HEADER_DESCRIPTION).getAsString() : "";
			descriptionHeader.add(sHeadDescription);

			JsonArray itemsArray = tvObject.get(VIDEO_ITEMS).getAsJsonArray();

			int nestedSize = itemsArray.size();
			List<String> nestedProgramList = new ArrayList<String>(nestedSize);
			List<String> nestedNetworkNameList = new ArrayList<String>(
					nestedSize);
			List<String> nestedPosterArtList = new ArrayList<String>(nestedSize);
			List<String> nestedVideoRatingList = new ArrayList<String>(
					nestedSize);
			List<String> nestedThumbList = new ArrayList<String>(nestedSize);
			List<String> nestedVidDescriptionList = new ArrayList<String>(
					nestedSize);
			List<String> nestedVidDurationList = new ArrayList<String>(
					nestedSize);

			for (int i = 0; i < nestedSize; i++) {
				String name = itemsArray.get(i).getAsJsonObject()
						.has(PROGRAM_NAME) ? itemsArray.get(i)
						.getAsJsonObject().get(PROGRAM_NAME).getAsString() : "";

				String network = itemsArray.get(i).getAsJsonObject()
						.has(NETWORK_NAME) ? itemsArray.get(i)
						.getAsJsonObject().get(NETWORK_NAME).getAsString() : "";

				String description = itemsArray.get(i).getAsJsonObject()
						.has(VIDEO_DESCRIPTION) ? itemsArray.get(i)
						.getAsJsonObject().get(VIDEO_DESCRIPTION).getAsString()
						: "";

				String duration = itemsArray.get(i).getAsJsonObject()
						.has(VIDEO_DURATION) ? itemsArray.get(i)
						.getAsJsonObject().get(VIDEO_DURATION).getAsString()
						: "";

				String art = itemsArray.get(i).getAsJsonObject()
						.has(POSTER_ART_URL) ? itemsArray.get(i)
						.getAsJsonObject().get(POSTER_ART_URL).getAsString()
						: "";

				String rating = (itemsArray.get(i).getAsJsonObject()
						.has(VIDEO_RATING)) ? itemsArray.get(i)
						.getAsJsonObject().get(VIDEO_RATING).getAsString()
						: UNKNOWN_RATING;

				String thumbnail = itemsArray.get(i).getAsJsonObject()
						.has(THUMB_URL) ? itemsArray.get(i).getAsJsonObject()
						.get(THUMB_URL).getAsString() : "";

				nestedProgramList.add(name);
				nestedNetworkNameList.add(network);
				nestedPosterArtList.add(art);
				nestedVideoRatingList.add(rating);
				nestedThumbList.add(thumbnail);
				nestedVidDescriptionList.add(description);
				nestedVidDurationList.add(duration);
			}
			vidName.add(nestedProgramList);
			networkName.add(nestedNetworkNameList);
			posterArt.add(nestedPosterArtList);
			vidRating.add(nestedVideoRatingList);
			vidThumb.add(nestedThumbList);
			vidDescription.add(nestedVidDescriptionList);
			vidDuration.add(nestedVidDurationList);
		}
		listVideoType = vidTypeHeader;
		listHeaderDescripton = descriptionHeader;
		listVideoName = vidName;
		listNetworkName = networkName;
		listPosterArt = posterArt;
		listVideoRating = vidRating;
		listThumb = vidThumb;
		listVideoDescription = vidDescription;
		listVideoDuration = vidDuration;
	}
}