package com.lmccarty.restexample;

import com.lmccarty.restexample.fragments.Details;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;

public class DetailsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		String[] params = extras.getStringArray(Main.VIDEO_DETAIL_VIEW);

		FragmentManager manager = getFragmentManager();
		FragmentTransaction trans = manager.beginTransaction();

		Details frag = new Details();

		Bundle bundle = new Bundle();
		bundle.putStringArray(Main.VIDEO_DETAIL_VIEW, params);
		frag.setArguments(bundle);

		trans.replace(android.R.id.content, frag).commit();
	}
}