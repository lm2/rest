package com.lmccarty.restexample;

import com.lmccarty.restexample.fragments.DataList;
import com.lmccarty.restexample.fragments.DataList.OnListItemClicked;
import com.lmccarty.restexample.fragments.Details;
import android.os.Bundle;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.view.View;

public class Main extends Activity implements OnListItemClicked {

	private static final String LIST = "list_fragment";
	public static final String DETAILS = "details_fragment";
	public static final String DESCRIPTION = "description";
	public static final String POSTER_ART = "posterArtwork";
	public static final String NETWORK_NAME = "networkName";
	public static final String TITLE = "title";
	public static final String VIDEO_DETAILS = "videoDetails";
	public static final String VIDEO_DETAIL_VIEW = "videoDetailView";
	public static final String VIDEO_DURATION = "videoDuration";
	public static final String VIDEO_RATING = "videoRating";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		addLayoutFrags();
	}

	private void addLayoutFrags() {

		Fragment fragList = getFragmentManager().findFragmentByTag(LIST);
		if (fragList == null || !fragList.getClass().equals(DataList.class)) {
			getFragmentManager().beginTransaction()
					.replace(R.id.container_main, new DataList(), LIST)
					.commit();
		}

		if (isRightInLayout()) {
			Fragment fragDetails = getFragmentManager().findFragmentByTag(
					DETAILS);
			if (fragDetails == null
					|| !fragDetails.getClass().equals(Details.class)) {
				getFragmentManager().beginTransaction()
						.replace(R.id.container_right, new Details(), DETAILS)
						.commit();
			}
		}
	}

	/**
	 * Checks to see if the app is being run a a phone or tablet.
	 * If the right frame is in layout, this is a tablet
	 * 
	 * @return true or false, true if this is a tablet, false
	 * if this is a phone
	 */
	private boolean isRightInLayout() {
		View rightFrame = findViewById(R.id.container_right);
		if (rightFrame == null) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * When the listview in DataList fragment is clicked, either an activity
	 * or a fragment is started, depending on the type of device.
	 * 
	 * @param params a number of strings with data to be displayed in the new fragment, or
	 * activity.
	 */
	@Override
	public void listItemClicked(String... params) {
		if (isRightInLayout()) {
			Details frag = (Details) getFragmentManager().findFragmentByTag(
					DETAILS);
			frag.setupUI(params);
		} else {
			startActivity(new Intent(this, DetailsActivity.class).putExtra(
					VIDEO_DETAIL_VIEW, params));
		}
	}
}